const Discord = require('discord.js');
const client = new Discord.Client();
const { prefix } = require('./config.json');

client.once('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setActivity(`Nicemcshop.com เว็บขายไอดีไมน์คราฟต์`, {
      type: "ONLINE",
    });
    
  });
           
client.on('message', message => {
	const args = message.content.slice(prefix.length).split(" ");
	const command = args.shift().toLowerCase();
	if (!message.content.startsWith(prefix) || message.author.bot) return;

	if(command === 'หักเงินเดือน') {
		const taggedUser = message.mentions.users.first()
		if (!taggedUser) {
		  return message.reply('สรุปใครโดนหัก?');
		}
		const amount = parseInt(args[1]);
	  
		  if (isNaN(amount)) {
			  return message.reply('กี่บาทล่ะ?');
		  }
		message.channel.send(`${taggedUser.username} โดนหักเงินเดือน ${amount} บาท`)
		} else if (command === 'เพิ่มเงินเดือน') {
			const amount = parseInt(args[1]);

			if (isNaN(amount)) {
				return message.reply('ไม่อยากเพิ่มก็หักแอดเจี๊ยบ');
			}
		  const taggedUser = message.mentions.users.first()
		  if (!taggedUser) {
		  return message.reply('ใครอ่ะ?');
		  }
		  message.channel.send(`${taggedUser.username} โดนเพิ่มเงินเดือนเป็น ${amount} บาท`)
		} else if (command === 'ช่วยด้วย') {
			message.channel.send("หักเงินเดือน เพิ่มเงินเดืิอน และ ช่วยด้วย")
		}
	}
)

client.login("token")